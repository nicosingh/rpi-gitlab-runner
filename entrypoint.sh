#!/bin/bash

# register gitlab runner if we are running for the first time
if ! ls /etc/gitlab-runner/config.toml
then
  gitlab-runner register
fi

# run service
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
gitlab-runner start

# sleep forever
sleep infinity
