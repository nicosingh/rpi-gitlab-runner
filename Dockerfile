FROM nicosingh/rpi-dind:latest

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

# download required software
RUN apt-get update -y && \
  apt-get -y install wget && \
  rm -rf /var/lib/apt/lists/*

# download gitlab runner
RUN wget -O /usr/local/bin/gitlab-runner https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-arm && \
  chmod +x /usr/local/bin/gitlab-runner

# add gitlab runner user
RUN useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# required variables for gitlab runner in docker
ENV RUNNER_NAME rpi-runner
ENV CI_SERVER_URL https://gitlab.com/
ENV REGISTRATION_TOKEN xxyyzz
ENV DOCKER_PRIVILEGED true
ENV DOCKER_VOLUMES "/var/run/docker.sock:/var/run/docker.sock"
ENV RUNNER_EXECUTOR docker
ENV DOCKER_IMAGE resin/rpi-raspbian
ENV REGISTER_NON_INTERACTIVE true
ENV RUNNER_TAG_LIST "rpi,raspberry,pi"

# entrypoint
COPY entrypoint.sh /sbin/
RUN chmod +x /sbin/entrypoint.sh
CMD ["/sbin/entrypoint.sh"]
